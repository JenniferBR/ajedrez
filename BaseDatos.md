create database ejemploError;

create table producto(
idProducto int primary key auto_increment,
cod_barras varchar(50),
nom_producto varchar(100),
des_producto varchar(100)
);

insert into producto values(null,'757568900','Fanta','Refresco de naranja'),
(null,'757689045','Tony col','Refresco de vainilla');

select * from producto;
insert into producto values (null,'757568900','sprite','Refresco de limon');

create Procedure proc_lanzarExcepcion(
in p_mensajeError varchar(250)
)
begin 
declare exit handler for sqlstate '4200'/*mandando llamar 4200 */
select p_mensajeError as 'Almacen_Error';
call raise_Error;/* mostrar ese mensaje atraves de otro procedure que se imprima*/
end;

create procedure proc_produto_actualizar(
in p_id_producto int, 
in p_cod_barras varchar(100),
in p_nom_producto varchar(50),
in p_producto varchar(100)
)
proc_producto_actualizar:begin

if p_cod_barras <> '' then 
if exists(
select 1 from producto
where cod_barras = p_cod_barras and id_producto <> p_id_producto
)then
call proc_lanzarExepcion('Ya existe un producto con el mismo codigo de barras');
leave proc_producto_actualizar;
end if;
end if;

if exists(
select 1 from producto
where nom_producto = p_cod_barras and id_producto <> p_id_producto

update producto 

end;